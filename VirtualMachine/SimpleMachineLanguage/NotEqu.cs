﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SVM.VirtualMachine;


namespace SVM.SimpleMachineLanguage
{
    class NotEqu : BaseInstructionWithOperand
    {
        public override void Run()
        {

            if (Operands[0].GetType() != typeof(string) || VirtualMachine.Stack.Count < 2)
            {
                throw new SvmRuntimeException(String.Format(BaseInstruction.OperandOfWrongTypeMessage,
                                                this.ToString()));
            }

            int first;
            if (VirtualMachine.Stack.Peek() is int)
            {
                first = (int)VirtualMachine.Stack.Pop();
                if (VirtualMachine.Stack.Peek() is int)
                {
                    if (first != (int)VirtualMachine.Stack.Peek())
                    {
                        VirtualMachine.Stack.Push(first);
                        VirtualMachine.ChangeLine(Operands[0]);
                    }
                }
                else
                {
                    throw new SvmRuntimeException(String.Format(BaseInstruction.OperandOfWrongTypeMessage,
                                                  this.ToString()));
                }
            }
            else
            {
                throw new SvmRuntimeException(String.Format(BaseInstruction.OperandOfWrongTypeMessage,
                                                   this.ToString()));
            }
        }
    }
}
