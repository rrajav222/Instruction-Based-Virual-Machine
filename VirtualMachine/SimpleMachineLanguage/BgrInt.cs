﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SVM.VirtualMachine;

namespace SVM.SimpleMachineLanguage
{
    class BgrInt:BaseInstructionWithOperand
    {
        public override void Run()
        {
            if (Operands.Length != 2 || VirtualMachine.Stack.Count < 1 || Operands[0].Length < 1 || Operands[1].Length < 1)
            {
                throw new SvmRuntimeException(String.Format(BaseInstruction.StackUnderflowMessage,
                                                this.ToString()));
            }
           
            if (Operands[0].GetType() != typeof(string) || Operands[1].GetType() != typeof(string))
            {
                throw new SvmRuntimeException(String.Format(BaseInstruction.OperandOfWrongTypeMessage,
                                                this.ToString()));
            }

            try
            {
                int topItem = (int)VirtualMachine.Stack.Pop();
                VirtualMachine.Stack.Push(topItem);
                if (int.TryParse(Operands[0], out int operand) && VirtualMachine.Stack.Peek() is int)
                {
                    if (operand > topItem)
                    {
                        VirtualMachine.ChangeLine(Operands[1]);
                    }
                }
                else
                {
                    throw new SvmRuntimeException(String.Format(BaseInstruction.OperandOfWrongTypeMessage,
                                                this.ToString()));
                }
            }
            catch
            {
                throw new SvmRuntimeException(String.Format(BaseInstruction.OperandOfWrongTypeMessage,
                                                this.ToString()));
            }
        }
    }
}
