﻿namespace SVM.VirtualMachine
{
    #region Using directives
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Diagnostics;

    #endregion
    /// <summary>
    /// Utility class which generates compiles a textual representation
    /// of an SML instruction into an executable instruction instance
    /// </summary>
    internal static class JITCompiler
    {
        #region Constants
        #endregion

        #region Fields
        #endregion

        #region Constructors
        #endregion

        #region Properties
        #endregion

        #region Public methods
        #endregion

        #region Non-public methods
        internal static IInstruction CompileInstruction(string opcode)
        {
            IInstruction instruction = null;

            #region TASK 1 - TO BE IMPLEMENTED BY THE STUDENT

            string extension;
            foreach (var file in Directory.EnumerateFiles(Environment.CurrentDirectory))
            {
                extension = Path.GetExtension(file);
                if (extension.ToLower() == ".dll")
                {
                    bool flag = false;
                    bool checkVerified = CheckStrongName.StrongNameSignatureVerification(file, false, ref flag);
                    if (!(flag && !checkVerified))
                    {
                        try
                        {
                            Assembly ass = Assembly.LoadFrom(file);
                            Type[] types = ass.GetTypes();
                            foreach (Type t in types)
                            {
                                if (t.Name.Contains(opcode, StringComparison.OrdinalIgnoreCase) && t.GetInterfaces().Contains(typeof(IInstruction)))
                                {
                                    instruction = (IInstruction)Activator.CreateInstance(t);
                                    break;
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }


            if (instruction == null)
            {
                throw new SvmCompilationException("Invalid SML instruction found in the SML source");
            }

            #endregion
            return instruction;
        }

        internal static IInstruction CompileInstruction(string opcode, params string[] operands)
        {
            IInstructionWithOperand instruction = null;

            #region TASK 1 - TO BE IMPLEMENTED BY THE STUDENT

            string extension;
            foreach (string file in Directory.EnumerateFiles(Environment.CurrentDirectory))
            {
                extension = Path.GetExtension(file);
                if (extension.ToLower() == ".dll")
                {
                    bool flag = false;
                    bool checkVerified = CheckStrongName.StrongNameSignatureVerification(file, false, ref flag);
                    if (!(flag && !checkVerified))
                    {
                        try
                        {
                            Assembly ass = Assembly.LoadFrom(file);
                            Type[] types = ass.GetTypes();
                            foreach (Type t in types)
                            {
                                if (t.Name.Contains(opcode, StringComparison.OrdinalIgnoreCase) && t.GetInterfaces().Contains(typeof(IInstruction)))
                                {
                                    instruction = (IInstructionWithOperand)Activator.CreateInstance(t);
                                    instruction.Operands = operands;
                                    break;
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }

            
            if (instruction == null)
            {
                throw new SvmCompilationException("Invalid SML instruction found in the SML source");
            }

            #endregion
            return instruction;
        }
        #endregion
    }
}
