﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SVM.VirtualMachine;
using SVM.VirtualMachine.Debug;

namespace Debuggers
{
    public class Debugger : IDebugger
    {
        #region TASK 5 - TO BE IMPLEMENTED BY THE STUDENT

        private IVirtualMachine PrivateVirtualMachine = null;
        public IVirtualMachine VirtualMachine { set => PrivateVirtualMachine = value; }
        public void Break(IDebugFrame debugFrame)
        {
            Console.WriteLine("Hello World");
        }

        #endregion
    }
}

