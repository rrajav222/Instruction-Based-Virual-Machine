﻿using System;
using System.IO;
using SVM.VirtualMachine;

namespace SML_Extensions
{
    public class LoadImage : BaseInstructionWithOperand
    {
        public override void Run()
        {
            string extension = Path.GetExtension(Operands[0]).ToLower();
            if (Operands[0].GetType() != typeof(string) || (extension != ".jpeg" && extension != ".png" && extension != ".jpg"))
            {
                throw new SvmRuntimeException(String.Format(BaseInstruction.OperandOfWrongTypeMessage,
                                                this.ToString(), VirtualMachine.ProgramCounter));
            }
            VirtualMachine.Stack.Push(Operands[0]);
        }
    }
}
