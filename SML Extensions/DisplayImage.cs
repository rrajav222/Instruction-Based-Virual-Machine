﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using SVM.VirtualMachine;
using System.Text;

namespace SML_Extensions
{
    public class DisplayImage : BaseInstruction
    {
        public override void Run()
        {
            try
            {
                if (VirtualMachine.Stack.Count < 1)
                {
                    throw new SvmRuntimeException(String.Format(BaseInstruction.StackUnderflowMessage,
                                                    this.ToString(), VirtualMachine.ProgramCounter));
                }
                string img = (string)VirtualMachine.Stack.Pop();
                string extension = Path.GetExtension(img).ToLower();

                if (extension != ".jpeg" && extension != ".png" && extension != ".jpg")
                {
                    throw new SvmRuntimeException(String.Format(BaseInstruction.OperandOfWrongTypeMessage,
                                                    this.ToString(), VirtualMachine.ProgramCounter));
                }

                Form form1 = new Form();
                form1.BackgroundImage = Image.FromFile(img);
                form1.Height = form1.BackgroundImage.Height;
                form1.Width = form1.BackgroundImage.Width;
                form1.ShowDialog();

            }
            catch (InvalidCastException)
            {
                throw new SvmRuntimeException(String.Format(BaseInstruction.OperandOfWrongTypeMessage,
                                                this.ToString(), VirtualMachine.ProgramCounter));
            }
        }
    }
}
